require 'rails_helper'

RSpec.describe SearchController, type: :controller do

  let(:doctor)            { instance_double("Doctor", :search => {:code => '200', :body => 'jsondata'}) }
  let(:doctor_with_error) { instance_double("Doctor", :search => {:code => '500', :body => 'error-response'}) }
  before :each do
    request.env['HTTP_ACCEPT'] = 'application/json'
  end

  describe 'GET #search' do

    it 'returns search response' do
      allow(Doctor).to receive(:new).and_return(doctor)
      get :search, name: 'Miller'
      expect(response).to have_http_status(:success)
      expect(response.body).to eq 'jsondata'
    end

    it 'returns status code' do
      allow(Doctor).to receive(:new).and_return(doctor_with_error)
      get :search, name: 'Miller'
      expect(response).to have_http_status(500)
      expect(response.body).to eq 'error-response'
    end
  end
end
