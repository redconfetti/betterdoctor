require 'rails_helper'

RSpec.describe Doctor, type: :model do

  before { Rails.cache.clear }

  describe '#search', :vcr => { allow_playback_repeats: true } do
    it 'returns 10 doctors' do
      response = subject.search('Miller')
      expect(response).to be_an_instance_of Hash
      expect(response[:code]).to be_an_instance_of Fixnum
      expect(response[:body]).to be_an_instance_of String
      result = JSON.parse(response[:body])
      expect(result['meta']).to be_an_instance_of Hash
      expect(result['meta']['data_type']).to eq 'array'
      expect(result['meta']['item_type']).to eq 'Doctor'
      expect(result['meta']['total']).to eq 2797
      expect(result['meta']['count']).to eq 10
      expect(result['meta']['skip']).to eq 0
      expect(result['meta']['limit']).to eq 10
      expect(result['data']).to be_an_instance_of Array
      expect(result['data'].count).to eq 10
      result['data'].each do |doctor|
        expect(doctor).to be_an_instance_of Hash
      end
      expect(result['data'][0]['profile']).to be_an_instance_of Hash
      expect(result['data'][0]['profile']['first_name']).to eq 'Javier'
      expect(result['data'][0]['profile']['last_name']).to eq 'Miller'
    end

    it 'caches result by name' do
      subject.search('Miller')
      expect(RestClient).to_not receive(:get)
      response = subject.search('Miller')
      expect(response).to be_an_instance_of Hash
      expect(response[:code]).to be_an_instance_of Fixnum
      expect(response[:body]).to be_an_instance_of String
      result = JSON.parse(response[:body])
      expect(result).to be_an_instance_of Hash
      expect(result['meta']).to be_an_instance_of Hash
      expect(result['data']).to be_an_instance_of Array
      expect(result['data'].count).to eq 10
    end
  end

end
