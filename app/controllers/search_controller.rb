class SearchController < ApplicationController
  def search
    doctor = Doctor.new
    search_results = doctor.search(params[:name])
    render json: search_results[:body], :status => search_results[:code]
  end
end
