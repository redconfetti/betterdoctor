class Doctor
  require 'json'
  require 'rest_client'

  BD_HOST_API = 'https://api.betterdoctor.com/'
  BD_API_VERSION = '2015-09-22'

  def search(name)
    Rails.cache.fetch("/doctors/#{name.to_s}") do
      response = RestClient.get "#{self.api_root}/doctors", {
        :params => {
          :name => name.to_s,
          :user_key => self.api_key
        }
      }
      {
        code: response.code,
        body: String.new(response) # See https://github.com/rest-client/rest-client/issues/370
      }
    end
  end

  def api_root
    BD_HOST_API + BD_API_VERSION
  end

  def api_key
    Figaro.env.better_doctor_api_key
  end
end
