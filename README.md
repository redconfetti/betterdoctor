# BetterDoctor Platform Engineer Coding Challenge

Thanks for your interest in joining the BetterDoctor Engineering team!

Your goal is to create a simple Rails API that proxies the BetterDoctor API. Why? For science!

We believe one of best ways to understand how an engineer thinks is to read through their code.

### Specifics

Implement a single endpoint in a rails api application:

/api/v1/doctors/search?name=<name to search for>

The api should be able to search the BetterDoctor API, by name, for a doctor using the 'name' field in the BetterDoctor API.

Cache any results for a given query. Pass all results for the given query back to the caller. Any given cache record should expire after 30 minutes.

Use Rails (4.2 or higher), Ruby (2.2 or higher) and a HTTP framework of your choosing.

Sign up at https://developer.betterdoctor.com to get access to the API.

Send us a GitHub link with your completed work. We'll take a tar.gz too...

Good luck and remember to show us your use of proper software engineering practices!

## Development Setup

[Install RVM](https://rvm.io/rvm/install)

Change to project directory with RVM loaded into environment to create 'betterdoctor' gemset.

Install Bundler gem, then run `bundle install`.

	$ gem install bundler
	$ bundle install

## Figaro Settings

This application uses Figaro to store and load configuration settings.

Using the `figaro` command, you can set values from your configuration file all at once:

```bash
$ figaro heroku:set -e production
```

For more information:

```bash
$ figaro help heroku:set
```

### Memcached

This project uses the [MemCachier service](https://devcenter.heroku.com/articles/memcachier) provided by Heroku in production.

### Contributor Notes

#### VCR Cassettes

To stub out the responses from the BetterDoctor API in Rspec tests, this project uses the VCR Cassettes gem.

[VCR 3.0.1 Docs](https://www.relishapp.com/vcr/vcr/v/3-0-1/docs/getting-started)

When creating new VCR recordings it is advised that you configure the correct API key within application.yml.example under the 'test' configuration. Run your specs using Rspec should cause an actual API call to be made to the remote server, with the response recorded in the spec/vcr_cassettes folder. After this recording is completed, modify the API key in the recording and in your application.yml, to ensure that the actual API key is not committed to the repository.

## References

* [REST Client Docs](http://www.rubydoc.info/gems/rest-client/1.8.0)
